package pos.machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItemList = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItemList);
        return renderReceipt(receipt);
    }
    public List<ReceiptItem> decodeToItems(List<String> barcodes){
        Map<String, Integer> barcodesWithQuantity = getBarcodesWithQuantity(barcodes);
        return getItemDetail(barcodesWithQuantity);
    }
    public Map<String,Integer> getBarcodesWithQuantity(List<String> barcodes){
        Map<String,Integer> barcodesMap = new HashMap<>();
        for (String barcode : barcodes) {
            Integer number = 1;
            if(barcodesMap.containsKey(barcode)){
                number = barcodesMap.get(barcode) + 1;
            }
            barcodesMap.put(barcode,number);
        }
        return barcodesMap;
    }

    public List<ReceiptItem> getItemDetail(Map<String, Integer> barcodesWithQuantity){
        List<ReceiptItem> receiptItemList = new ArrayList<>();
        List<Item> items = ItemsLoader.loadAllItems();
        for (String barcode : barcodesWithQuantity.keySet()) {
            for (Item item : items) {
                if(barcode.contains(item.getBarcode())){
                    ReceiptItem receiptItem = new ReceiptItem(item.getName(),barcodesWithQuantity.get(barcode),item.getPrice());
                    receiptItemList.add(receiptItem);
                }
            }
        }
        return receiptItemList;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItemList){
        int totalPrice = calculateTotalPrice(receiptItemList);
        return new Receipt(receiptItemList,totalPrice);
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItemList){
        int totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItemList) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }

    public String renderReceipt(Receipt receipt){
        String itemsReceipt = generateItemsReceipt(receipt);
        return generateReceipt(itemsReceipt,receipt.totalPrice);
    }

    public String generateItemsReceipt(Receipt receipt){
        String itemsReceipt = "";
        for (ReceiptItem receiptItem : receipt.receiptItemList) {
            itemsReceipt += "Name: " + receiptItem.toString() + "\n";
        }
        return itemsReceipt;
    }

    public String generateReceipt(String itemsReceipt,int totalPrice){
        String receipt = "***<store earning no money>Receipt***\n";
        receipt += itemsReceipt;
        receipt += "----------------------\nTotal: "+ totalPrice + " (yuan)\n**********************";
        return receipt;
    }

}
